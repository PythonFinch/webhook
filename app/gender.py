import random

def create(file, array):
    with open(file, 'r', encoding="utf8") as f:
        for i in f:
            array.append(i.lower().strip())

def get_gender():
    prefix = ['а', 'транс*', 'пан', 'кибер', 'деми', 'поли', 'цис', 'гомо', 'гетеро', 'би', 'кило', 'мега', 'гига', 'тера', 'деци', 'санти', 'милли', 'микро', 'нано', 'макро', 'супер', 'ультра']
    suffix = ['гендер', 'флюид']
    nouns = []
    adjs = []
    profs = []
    create('./misc/russian_nouns.txt', nouns)
    create('./misc/adjectives.txt', adjs)
    create('./misc/professions.txt', profs)
    name = ''
    if random.randint(0, 1):
        name = random.choice(prefix)
    if random.randint(0, 100) < 15:
        name = name + random.choice(nouns) + '-'
    name = (name + (random.choice(nouns) + random.choice(suffix))).capitalize()
    prof = random.choice(profs) + '_' + random.choice(['ка', 'ша', 'есса', 'иня', 'ица', 'иса', 'а'])
    result = '{} — гендер {} и {}, ощущается как {}. ' \
             'Чувствуешь себя то как {}, а то и как {}. ' \
             'Если ты {}, то скорее всего, ты и есть {}'.format(name,
                                                                random.choice(adjs),
                                                                random.choice(adjs),
                                                                random.choice(adjs),
                                                                random.choice(nouns),
                                                                random.choice(nouns),
                                                                prof,
                                                                name.lower())
    return result