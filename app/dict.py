url = 'https://ru.wiktionary.org/w/index.php?title={}&action=raw'
from bs4 import BeautifulSoup
import re, requests

print('Что ищем?')
x = input()
try:
    page = requests.get(url.format(x.lower()))
    soup = BeautifulSoup(page.text, 'lxml')
    raw = soup.find('body').text
    result = re.search(r'==== Значение ====(.+?)====', raw, flags=re.S)
    definition = result.group(1)
    definition = re.sub('\|[a-z]{2}', '', definition)
    definition = re.sub('[а-яА-Я]*\|', '', definition)
    definition = re.sub(r'# \n', '', definition)
    definition = definition.replace('[[', '').replace(']]', '').replace('}}', '').replace('{{', '')
    print(definition.strip())
except AttributeError:
    print('Нет такого слова «{}!»'.format(x))

