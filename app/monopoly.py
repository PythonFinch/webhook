#!/usr/bin/python3.7

import check_txt
import sqlunit
import achievements
from random import choice, randint


def write_data(increment, message, bot):
    userid = message.from_user.id
    current_moons = sqlunit.read_db(column_name='id',
                                    row_id=userid)[0][3]
    sqlunit.update_data(id_column='id',
                        record=userid,
                        param_column='Moons',
                        param_val=current_moons + increment
                        )
    achievements.update_auto_ach(ach_id=2,
                                 user_id=userid,
                                 message=message,
                                 bot=bot,
                                 increment=increment)
    return increment


def communism(message, bot):
    bank = check_txt.moon_bank()
    pidors = sqlunit.pidors_list()
    how_much = bank // len(pidors)
    for pidor in pidors:
        sqlunit.update_data(id_column='id',
                            record=pidor[0],
                            param_column='Moons',
                            param_val=how_much
                            )
        achievements.update_auto_ach(ach_id=2,
                                     user_id=pidor[0],
                                     message=message,
                                     bot=bot,
                                     make_null=True)
        achievements.update_auto_ach(ach_id=2,
                                     user_id=pidor[0],
                                     message=message,
                                     bot=bot,
                                     increment=how_much)
    return "Наступил коммунизм! Всё отнять и поделить! Ура, товарищи!"


def birthday(message, bot):
    userid = message.from_user.id
    pidors = sqlunit.pidors_list()
    who = sqlunit.read_db(column_name='id',
                          row_id=userid)[0][3]
    for pidor in pidors:
        if pidor[0] != userid:
            gift = pidor[3] // 10
            sqlunit.update_data(id_column='id',
                                record=pidor[0],
                                param_column='Moons',
                                param_val=pidor[3] - gift
                                )
            achievements.update_auto_ach(ach_id=2,
                                         user_id=pidor[0],
                                         message=message,
                                         bot=bot,
                                         increment=-gift)
            who += gift

    sqlunit.update_data(id_column='id',
                        record=userid,
                        param_column='Moons',
                        param_val=who
                        )
    achievements.update_auto_ach(ach_id=2,
                                 user_id=userid,
                                 message=message,
                                 bot=bot,
                                 make_null=True)
    achievements.update_auto_ach(ach_id=2,
                                 user_id=userid,
                                 message=message,
                                 bot=bot,
                                 increment=who)
    return "У тебя день рождения! Все готовы отсыпать тебе своих лун!"


def error(message, bot):
    return f"Из-за ретроградного меркурия получаешь {write_data(randint(50, 100), message, bot)} лун!!"


def inheritance(message, bot):
    return f"Лично от бога луны Ааха получаешь {write_data(100, message, bot)} лун!!"


def fine(message, bot):
    return f"Штраф {write_data(randint(-100, -50), message, bot)} лун за вождение себя по улице в пьяном виде!"


def rent(message, bot):
    return f"Пора платить аренду! Минус {write_data(-150, message, bot)} лун, а как вы хотели?"


def mmm(message, bot):
    return f"В результате вложения в МММ-2019, баланс лун изменился на {write_data(randint(-100, 100), message, bot)}. Это ещё повезло!"


def course_down(message, bot):
    return f"Из-за падения курса теряешь {write_data(randint(-100, -50), message, bot)} лун. Надо было слушать копустена и покупать биткоены!"


def course_up(message, bot):
    return f"Отскок штоле? Получаешь {write_data(randint(50, 100), message, bot)} лун из-за внезапного укрепления национальной луны."


def sanctions(message, bot):
    pidors = sqlunit.pidors_list()
    for pidor in pidors:
        fine = pidor[3] // 15
        sqlunit.update_data(id_column='id',
                            record=pidor[0],
                            param_column='Moons',
                            param_val=pidor[3] - fine
                            )
        achievements.update_auto_ach(ach_id=2,
                                     user_id=pidor[0],
                                     message=message,
                                     bot=bot,
                                     increment=-fine)
    return "Новый пакет санкций подъехал. Только на пользу!"


def percent_fine(message, bot):
    userid = message.from_user.id
    current = sqlunit.read_db(column_name='id',
                              row_id=userid)[0][3]
    sudden_fine = current // randint(3, 10)
    sqlunit.update_data(id_column='id',
                        record=userid,
                        param_column='Moons',
                        param_val=current - sudden_fine
                        )
    achievements.update_auto_ach(ach_id=2,
                                 user_id=userid,
                                 message=message,
                                 bot=bot,
                                 increment=-sudden_fine)

    return f"Ярик, бачок потiк! На ремонт бачка ушло {sudden_fine} лун."


def random_event(message, bot, exact=-1):
    events = (communism, error, inheritance,
              fine, rent, mmm, course_down,
              course_up, sanctions, birthday,
              percent_fine)
    if exact == -1:
        to_send = choice(events)(message, bot)
    else:
        to_send = events[exact](message, bot)
    bot.send_photo(message.chat.id, open('./misc/monopoly.jpg', 'rb'), caption=to_send)
