# -*- coding: utf-8 -*-

import sqlite3


def create_new_db(path='./misc/data.db'):
    sql_create_parse_table = ("CREATE TABLE IF NOT EXISTS users(id INTEGER PRIMARY KEY,"
                              "Nick TEXT,"
                              "RealName TEXT,"
                              "Moons INTEGER,"
                              "IsKing INTEGER,"
                              "MonthKing INTEGER,"
                              "TotalKing INTEGER"
                              ")")

    with sqlite3.connect(path) as database:
        cursor = database.cursor()

        cursor.execute(sql_create_parse_table)


def create_ach_db(path='./achievements/achievements.db'):
    sql_create_parse_table = ("CREATE TABLE IF NOT EXISTS users(id INTEGER PRIMARY KEY,"
                              "FromUser TEXT,"
                              "ToUser TEXT,"
                              "Avatar TEXT,"
                              "Title TEXT,"
                              "Body TEXT,"
                              "Date INTEGER"
                              ")")

    with sqlite3.connect(path) as database:
        cursor = database.cursor()

        cursor.execute(sql_create_parse_table)


def read_db(column_name='id', row_id=None, path='./misc/data.db', table='users'):
    with sqlite3.connect(path) as database:
        cursor = database.cursor()
        if row_id is None:
            query = 'SELECT * FROM ' + table
            cursor.execute(query)
        else:
            query = 'SELECT * FROM ' + table + ' WHERE {}={}'.format(column_name, row_id)
            cursor.execute(query)
        data = cursor.fetchall()
    return data


def add_data(data, path='./misc/data.db', table='users'):
    with sqlite3.connect(path) as database:
        cursor = database.cursor()
        var_string = ', '.join('?' * len(data))
        query_string = 'INSERT OR IGNORE INTO users VALUES (%s);' % var_string
        query_string = query_string.replace('users', table)
        cursor.execute(query_string, data)
        database.commit()


def del_data(id_column, record, path='./misc/data.db', table='users'):
    with sqlite3.connect(path) as database:
        cursor = database.cursor()
        query = 'DELETE FROM ' + table + ' WHERE ' + id_column + " = '" + record + "'"
        cursor.execute(query)
        database.commit()


def update_data(id_column, record, param_column, param_val, path='./misc/data.db', table='users'):
    with sqlite3.connect(path) as database:
        cursor = database.cursor()
        query = 'UPDATE ' + table + ' SET ' + param_column + '= "' + str(
            param_val) + '" WHERE ' + id_column + " = '" + str(record) + "'"
        cursor.execute(query)
        database.commit()


def max(column, path='./misc/data.db', table='users', param='IsPidor'):
    with sqlite3.connect(path) as database:
        cursor = database.cursor()
        query = 'SELECT MAX({}) from {} WHERE {}=1'.format(column, table, param)
        cursor.execute(query)
        data = cursor.fetchall()
        return data


def pidors_list(path='./misc/data.db'):
    with sqlite3.connect(path) as database:
        cursor = database.cursor()
        query = 'SELECT * from users WHERE IsPidor=1'
        cursor.execute(query)
        data = cursor.fetchall()
        return data


def read_user_auto_ach(ach_id, user_id, path='./misc/data.db', table='users'):
    with sqlite3.connect(path) as database:
        cursor = database.cursor()
        query = 'SELECT * FROM ' + table + ' WHERE ach_id={} AND user_id={}'.format(ach_id, user_id)
        cursor.execute(query)
        data = cursor.fetchall()
    return data
