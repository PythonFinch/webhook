#!/usr/bin/python3.7

import telebot
from misc import token, dt, hook
from flask import Flask, request
import requests
import random
from traceback import format_exc
import sqlunit
import gender
import pickle
import inspect
import dropbox
import achievements
import check_txt
import time
from telebot.types import InputMediaPhoto

URL = 'https://api.telegram.org/bot' + token + '/'

pa_url = 'https://PythonFinch.pythonanywhere.com/' + hook


class BotWithShutUp(telebot.TeleBot):
    def send_message(self, chat_id, text, disable_web_page_preview=None, reply_to_message_id=None, reply_markup=None,
                     parse_mode=None, disable_notification=None):
        row = sqlunit.read_db(column_name='Name',
                              row_id='"silent"',
                              path='./misc/data.db',
                              table='service')
        if row[0][2] == '1':
            super().send_message(chat_id,
                                 'У меня обед! То есть, обет молчания. /shutup для выключения {}'.format(
                                     inspect.stack()[1][3]),
                                 disable_web_page_preview=None,
                                 reply_to_message_id=None,
                                 reply_markup=None,
                                 parse_mode=None,
                                 disable_notification=None)
            return

        super().send_message(chat_id, text,
                             disable_web_page_preview=disable_web_page_preview,
                             reply_to_message_id=reply_to_message_id,
                             reply_markup=reply_markup,
                             parse_mode=parse_mode,
                             disable_notification=disable_notification)

    def send_sticker(self, chat_id, data, reply_to_message_id=None, reply_markup=None, disable_notification=None,
                     timeout=None):
        row = sqlunit.read_db(column_name='Name',
                              row_id='"silent"',
                              path='./misc/data.db',
                              table='service')
        if row[0][2] == '1':
            super().send_message(chat_id, 'У меня обед! То есть, обет молчания. /shutup для выключения',
                                 disable_web_page_preview=None,
                                 reply_to_message_id=None,
                                 reply_markup=None,
                                 parse_mode=None,
                                 disable_notification=None)
            return

        super().send_sticker(chat_id, data,
                             reply_to_message_id=None,
                             reply_markup=None,
                             disable_notification=None,
                             timeout=None)

    def send_photo(self, chat_id, photo, caption=None, reply_to_message_id=None, reply_markup=None,
                   parse_mode=None, disable_notification=None):
        row = sqlunit.read_db(column_name='Name',
                              row_id='"silent"',
                              path='./misc/data.db',
                              table='service')
        if row[0][2] == '1':
            super().send_message(chat_id, 'У меня обед! То есть, обет молчания. /shutup для выключения',
                                 disable_web_page_preview=None,
                                 reply_to_message_id=None,
                                 reply_markup=None,
                                 parse_mode=None,
                                 disable_notification=None)
            return
        super().send_photo(chat_id, photo,
                           caption=caption,
                           reply_to_message_id=None,
                           reply_markup=None,
                           parse_mode=None,
                           disable_notification=None)


bot = BotWithShutUp(token, threaded=False)
bot.remove_webhook()
bot.set_webhook(url=pa_url)


def error_message(id_to_send):
    bot.send_message(id_to_send, 'Ошибка {} в модуле {}. Бери вилку и чини! Разразраз'.format(format_exc(),
                                                                                              inspect.stack()[1][3]))


app = Flask(__name__)


@app.route('/{}'.format(hook), methods=['POST'])
def webhook():
    update = telebot.types.Update.de_json(request.stream.read().decode('utf-8'))
    current_time = int(time.time())
    current_update_id = update.update_id

    row = sqlunit.read_db(column_name='id',
                          row_id=2,
                          path='./misc/data.db',
                          table='service')
    last_time = int(row[0][2].split('_')[0])
    last_update_id = int(row[0][2].split('_')[1])

    # bot.send_message(69934849, f'Current: {current_time} \n Last: {last_time}')

    if (current_time - last_time > 2) and current_update_id > last_update_id:
        sqlunit.update_data(id_column='id',
                            record=2,
                            param_column='Value',
                            param_val=f'{current_time}_{current_update_id}',
                            table='service')
        bot.process_new_updates([update])
    elif update.message and update.message.json and "text" in update.message.json:
        if update.message.json["text"].startswith('/') and (current_time - last_time <= 2):
            bot.send_message(update.message.json["chat"]["id"], 'Дохуя хочешь!')
    return 'ok', 200


@bot.message_handler(commands=['start'])
def start(message):
    try:
        bot.send_message(message.chat.id, 'Ну что ж')
    except:
        error_message(message.chat.id)


@bot.message_handler(commands=['cat', 'dog', 'dick', 'papug'])
def cat(message):
    try:
        word, space, rest = message.text.partition(' ')
        word = word.split('@')[0]
        dbx = dropbox.Dropbox(dt)
        cats_list = [file.name for file in dbx.files_list_folder('/{}S'.format(word.upper()), recursive=True).entries if
                     file.name != '{}S'.format(word.upper())]
        filename = '{}S/'.format(word.upper()) + random.choice(cats_list)
        if filename:
            with open("./misc/temppic.jpg", "wb") as f:
                metadata, res = dbx.files_download(path=filename)
                f.write(res.content)
        bot.send_photo(message.chat.id, open('./misc/temppic.jpg', 'rb'))
        '''if filename:
            link = dbx.sharing_create_shared_link(filename)
        bot.send_photo(message.chat.id, link.url)'''
        achievements.update_auto_ach(15, message.from_user.id, message, bot, increment=1)

        row = sqlunit.read_db(column_name='id',
                              row_id=2,
                              path='./misc/data.db',
                              table='service')
        current_time = int(time.time())
        current_update_id = int(row[0][2].split('_')[1])

        sqlunit.update_data(id_column='id',
                            record=2,
                            param_column='Value',
                            param_val=f'{current_time}_{current_update_id}',
                            table='service')
    except:
        bot.send_photo(message.chat.id,
                       'https://www.dropbox.com/s/xipvuoihh8vniso/abac8573ca3c2465b553a2cc0ac61a51.jpg?dl=0')
        error_message('69934849')


@bot.message_handler(commands=['weather'])
def weather(message):
    try:
        url = 'http://api.openweathermap.org/data/2.5/weather?id=1498693&appid=f82fa6cb8bf62c330c3296035e916072'
        response = requests.get(url).json()
        temp = response['main']['temp']
        result = str(round(temp - 273))

        word, space, rest = message.text.partition(' ')
        if 'Минусинск' in rest:
            bot.send_message(message.chat.id,
                             'Температура воздуха в г. Минусинск {} °C.'.format(result))
        else:
            bot.send_message(message.chat.id,
                             '{}? Хуй там! Температура воздуха в г. Минусинск {} °C.'.format(rest, result))
    except:
        error_message(message.chat.id)


@bot.message_handler(commands=['death'])
def death(message):
    years = random.randint(10, 70)
    sec = years * 365 * 24 * 60 * 60 + random.randint(0, 999)
    achievements.update_auto_ach(23, message.from_user.id, message, bot, increment=1)
    deaths = []
    gender.create('./misc/ways_to_die.txt', deaths)
    bot.send_message(message.chat.id,
                     'Я думаю, ты умрёшь через {} сек или {} ку. {}'.format(check_txt.add_separator(sec), years,
                                                                            random.choice(deaths).capitalize()))


@bot.message_handler(commands=['taro'])
def taro(message):
    try:
        your_taro = random.randint(0, 21)
        if your_taro < 10:
            taro_txt = '0' + str(your_taro)
        else:
            taro_txt = your_taro
        achievements.update_auto_ach(22, message.from_user.id, message, bot)
        bot.send_photo(message.chat.id, open('./taro/{}.png'.format(your_taro), 'rb'),
                       caption='Вот твоя судьба:\n\nhttp://taroonline.narod.ru/{}.html'.format(taro_txt))
    except:
        error_message(message.chat.id)


@bot.message_handler(commands=['moonbank'])
def moon_bank(message):
    try:
        bot.send_message(message.chat.id,
                         'Всего в банке {} лун.'.format(check_txt.add_separator(check_txt.moon_bank())))
    except:
        error_message(message.chat.id)


@bot.message_handler(commands=['help'])
def help_me(message):
    try:
        with open('./misc/help.txt', 'r', encoding='utf-8') as file:
            bot.send_message(message.chat.id, file.read())
    except:
        error_message(message.chat.id)


@bot.message_handler(commands=['allusers'])
def all_users(message):
    try:
        data = sqlunit.read_db()
        to_send = '\n'.join(map(str, data)) + '\n Всего записей: ' + str(len(data))
        bot.send_message(message.chat.id, to_send)
    except:
        error_message(message.chat.id)


@bot.message_handler(commands=['anek'])
def anecdote(message):
    try:
        with open('./misc/anek.pickle', 'rb') as f:
            anecdotes = pickle.load(f)
        bot.send_message(message.chat.id, random.choice(tuple(anecdotes)))
        achievements.update_auto_ach(14, message.from_user.id, message, bot, increment=1)
    except:
        error_message(message.chat.id)


@bot.message_handler(commands=['podd'])
def p_odd(message):
    try:
        row = sqlunit.read_db(column_name='Name',
                              row_id='"podd"',
                              path='./misc/data.db',
                              table='service')
        achievements.update_auto_ach(24, message.from_user.id, message, bot, increment=1)
        bot.send_message(message.chat.id,
                         'Напоминаю: {} дня — {}! #незабудем #можемповторить'.format(
                             random.choice(('пидор', 'нурсултан', 'рапопорт')), row[0][2]))
    except:
        error_message(message.chat.id)


@bot.message_handler(commands=['addpidor'])
def add_king(message):
    try:
        word, space, rest = message.text.partition(' ')
        db = sqlunit.read_db()
        found = False
        for record in db:
            if record[1] == rest:
                found = True
                sqlunit.update_data(id_column='id',
                                    record=record[0],
                                    param_column='IsPidor',
                                    param_val=1)
                sqlunit.update_data(id_column='id',
                                    record=record[0],
                                    param_column='Moons',
                                    param_val=0)
                sqlunit.update_data(id_column='id',
                                    record=record[0],
                                    param_column='WordsMonth',
                                    param_val=0)
                sqlunit.update_data(id_column='id',
                                    record=record[0],
                                    param_column='WordsTotal',
                                    param_val=0)
                bot.send_message(message.chat.id,
                                 'Теперь {} участвует в жеребьёвке пидоров!'.format(record[1]))
                auto_achs = sqlunit.read_db(path='./misc/data.db', table='hc_ach')
                for auto_ach in auto_achs:
                    data_to_add = (None, record[0], record[1], auto_ach[0], auto_ach[1], 0, 0)
                    sqlunit.add_data(data_to_add,
                                     path='./misc/data.db',
                                     table='hc_ach_users')
        if not found:
            bot.send_message(message.chat.id,
                             'Нет такого!')
    except:
        error_message(message.chat.id)


@bot.message_handler(commands=['delpidor'])
def del_king(message):
    try:
        word, space, rest = message.text.partition(' ')
        db = sqlunit.read_db()
        found = False
        for record in db:
            if record[1] == rest:
                found = True
                sqlunit.update_data(id_column='id',
                                    record=record[0],
                                    param_column='IsPidor',
                                    param_val=0)
                bot.send_message(message.chat.id,
                                 'Теперь {} НЕ участвует в жеребьёвке пидоров!'.format(record[1]))
                sqlunit.del_data(id_column='user_id',
                                 record=str(record[0]),
                                 path='./misc/data.db',
                                 table='hc_ach_users')
        if not found:
            bot.send_message(message.chat.id, 'Нет такого!')
    except:
        error_message(message.chat.id)


@bot.message_handler(content_types=['photo'])
def achievement(message):
    try:
        if not message.caption or not message.caption.startswith('ачивка'):
            achievements.update_auto_ach(28, message.from_user.id, message, bot, increment=1)
            return
        achievements.achievement(message, bot)
    except:
        error_message(message.chat.id)


@bot.message_handler(content_types=['sticker'])
def lama(message):
    try:
        # bot.send_message(message.chat.id, message)
        if message.sticker.file_id == 'CAADAgADmF0BAAFji0YMBEWmcb0gMocWBA':
            bot.send_sticker(message.chat.id, 'CAADAgADmF0BAAFji0YMBEWmcb0gMocWBA')
            achievements.update_auto_ach(7, message.from_user.id, message, bot)

        if message.sticker.file_id == check_txt.lama_code:
            bot.send_sticker(message.chat.id, check_txt.lama_code)
            achievements.update_auto_ach(6, message.from_user.id, message, bot)
    except:
        error_message(message.chat.id)


@bot.message_handler(content_types=['voice'])
def voice(message):
    try:
        bot.send_sticker(message.chat.id, random.choice(('CAADAgADKgADAR8rBEIz0RCFEbznFgQ',
                                                         'CAADAgADKwADAR8rBF-x4myrDrM2FgQ',
                                                         'CAADAgADLAADAR8rBPSuk1D57zcPFgQ',
                                                         'CAADAgADLQADAR8rBE3IAAGWgkSqwRYE',
                                                         'CAADAgADLgADAR8rBPyvu8PekbqQFgQ',
                                                         'CAADAgADLwADAR8rBBJWqEeJbKrmFgQ',
                                                         'CAADAgADMAADAR8rBJWSw8dA3jvNFgQ',
                                                         'CAADAgADMQADAR8rBBwnMwi3OqxlFgQ',
                                                         'CAADAgADMgADAR8rBB1GiXv4raSvFgQ',
                                                         'CAADAgADMwADAR8rBJrrLtGlvLG6FgQ',)))
        achievements.update_auto_ach(16, message.from_user.id, message, bot, increment=1)
    except:
        error_message(message.chat.id)


@bot.message_handler(commands=['lolwut'])
def lol_wut(message):
    try:
        '''with open('./misc/main_message.pickle', 'wb') as f:
            pickle.dump(message, f)
        bot.send_message(message.chat.id, 'Потому что нехуй забывать, где ты живёшь')'''
        bot.send_document(message.chat.id, 'https://i.imgur.com/eHc58s3.mp4')
        pass
    except:
        error_message(message.chat.id)


@bot.message_handler(commands=['sendmsg'])
def send_msg(message):
    try:
        word, space, rest = message.text.partition(' ')
        bot.send_message('-1001457079924', rest)
    except:
        error_message(message.chat.id)


@bot.message_handler(commands=['ktopizdit', 'ktopizditmax', 'ktopidormax'])
def who_talks(message):
    try:
        data = sqlunit.read_db()
        msg = ''
        pizdict = {}
        for row in data:
            if row[4] == 1:
                if message.text == '/ktopizdit' or message.text == '/ktopizdit@moonpietestbot':
                    pizdict[row[1][1:]] = row[7]
                elif message.text == '/ktopidormax' or message.text == '/ktopidormax@moonpietestbot':
                    pizdict[row[1][1:]] = row[6]
                else:
                    pizdict[row[1][1:]] = row[8]
        sorted_pizdict = sorted(pizdict.items(), key=lambda kv: kv[1], reverse=True)
        for i in sorted_pizdict:
            if message.text == '/ktopizdit' or message.text == '/ktopizdit@moonpietestbot':
                if i[1]:
                    msg += '{} за месяц напиздел(а) {} словей.\n'.format(i[0], i[1])
                else:
                    msg += '{} молчит как сыч.\n'.format(i[0])
            elif message.text == '/ktopidormax' or message.text == '/ktopidormax@moonpietestbot':
                msg += '{} за всё время был(а) пидором {} раз(а).\n'.format(i[0], i[1])
            else:
                if i[1]:
                    msg += '{} за всё время напиздел(а) {} словей.\n'.format(i[0], i[1])
                else:
                    msg += '{} молчит как сыч.\n'.format(i[0])
        bot.send_message(message.chat.id, msg)
    except:
        error_message(message.chat.id)


@bot.message_handler(commands=['horo'])
def horoscope(message):
    try:
        keyboard = telebot.types.InlineKeyboardMarkup()
        keyboard.row(telebot.types.InlineKeyboardButton('Овн', callback_data='horo_aries'),
                     telebot.types.InlineKeyboardButton('Тлц', callback_data='horo_taurus'),
                     telebot.types.InlineKeyboardButton('Блзнц', callback_data='horo_gemini'),
                     telebot.types.InlineKeyboardButton('Рк', callback_data='horo_cancer'),
                     telebot.types.InlineKeyboardButton('Лв', callback_data='horo_leo'),
                     telebot.types.InlineKeyboardButton('Дв', callback_data='horo_virgo'))
        keyboard.row(telebot.types.InlineKeyboardButton('Вс', callback_data='horo_libra'),
                     telebot.types.InlineKeyboardButton('Скрпн', callback_data='horo_scorpio'),
                     telebot.types.InlineKeyboardButton('Стрлц', callback_data='horo_sagittarius'),
                     telebot.types.InlineKeyboardButton('Кзрг', callback_data='horo_carpicorn'),
                     telebot.types.InlineKeyboardButton('Вдл', callback_data='horo_aquarius'),
                     telebot.types.InlineKeyboardButton('Рб', callback_data='horo_pisces'))

        bot.send_message(message.chat.id, 'Ты кто по жизни-то?', reply_markup=keyboard)

    except:
        error_message(message.chat.id)


@bot.message_handler(commands=['set_gab'])
def set_gab(message):
    try:
        keyboard = telebot.types.InlineKeyboardMarkup()
        keyboard.row(telebot.types.InlineKeyboardButton('ой бля точно, не та команда', callback_data='gab_cancel'))
        keyboard.row(telebot.types.InlineKeyboardButton('0', callback_data='gab_0'),
                     telebot.types.InlineKeyboardButton('1', callback_data='gab_1'),
                     telebot.types.InlineKeyboardButton('2', callback_data='gab_2'),
                     telebot.types.InlineKeyboardButton('3', callback_data='gab_3'),
                     telebot.types.InlineKeyboardButton('4', callback_data='gab_4'))
        keyboard.row(telebot.types.InlineKeyboardButton('5', callback_data='gab_5'),
                     telebot.types.InlineKeyboardButton('6', callback_data='gab_6'),
                     telebot.types.InlineKeyboardButton('7', callback_data='gab_7'),
                     telebot.types.InlineKeyboardButton('8', callback_data='gab_8'),
                     telebot.types.InlineKeyboardButton('9', callback_data='gab_9'),
                     telebot.types.InlineKeyboardButton('10', callback_data='gab_10'))

        bot.send_message(message.chat.id, 'ПОДУМОЙ! А ТО БУДЕТ КАК В ТОТ РАЗ!', reply_markup=keyboard)
    except:
        error_message(message.chat.id)


@bot.callback_query_handler(func=lambda call: True)
def iq_callback(query):
    try:
        data = query.data
        if data.startswith('gab_'):
            if data == 'gab_cancel':
                bot.answer_callback_query(query.id)
                bot.send_message(query.message.chat.id,
                                 'Понял принял! В этот раз без штрафа, но впредь будьте аккуратнее!')
                return
            bot.answer_callback_query(query.id)
            bot.send_message(query.message.chat.id, 'Параметр разговорчивости теперь {}!'.format(query.data[4:]))
            sqlunit.update_data(id_column='Name',
                                record='"gab"',
                                param_column='Value',
                                param_val=query.data[4:],
                                table='service')

        if data.startswith('horo_'):
            urll = 'http://www.astrolive.ru/daily/erotic/'
            bot.answer_callback_query(query.id)
            bot.send_message(query.message.chat.id,
                             'Вот твой гороскоп на сегодня, дитя 21 века:\n\n{}{}.html'.format(urll, query.data[5:]))
    except:
        error_message(query.message.chat.id)


@bot.message_handler(commands=['set_percent'])
def set_percent(message):
    try:
        can_send = False
        admin_list = bot.get_chat_administrators(-1001457079924)
        for admin in admin_list:
            if message.from_user.id == admin.user.id:
                can_send = True
                break
        if not can_send:
            bot.send_message(message.chat.id, 'Я тебе этого не разрешаю, живи с этим.')
            return
        word, space, rest = message.text.partition(' ')
        if not rest.isdigit() or int(rest) > 100 or int(rest) < 0:
            bot.send_message(message.chat.id, 'Процент — это от нуля до ста, ну алоэ')
            return
        sqlunit.update_data(id_column='Name',
                            record='"percent"',
                            param_column='Value',
                            param_val=rest,
                            table='service')

        bot.send_message(message.chat.id, 'Буду щитпостить с вероятностью {} процентов'.format(rest))
    except:
        error_message(message.chat.id)


@bot.message_handler(commands=['get_stat'])
def get_stat(message):
    try:
        prep = ['.', '!', '?']
        # Get raw text as string.
        with open('./misc/history.txt', encoding="utf8") as f:
            text1 = f.read()
            text1 = text1.split(' ')
        with open('./misc/sorokin.txt', encoding="utf8") as f:
            text2 = f.read()
            text2 = text2.split(' ')
        with open('./misc/pairs.pickle', 'rb') as f:
            pairs = pickle.load(f)
        for i in range(0, len(text1) - 1):
            try:
                find = text1[i] + ' ' + text1[i + 1]
                if not text1[i][-1] in prep:
                    pairs.add(find)
            except IndexError:
                continue
        for i in range(0, len(text2) - 1):
            try:
                find = text2[i] + ' ' + text2[i + 1]
                if not text2[i][-1] in prep:
                    pairs.add(find)
            except IndexError:
                continue
        with open('./misc/pairs.pickle', 'wb') as f:
            pickle.dump(pairs, f)

        bot.send_message(message.chat.id,
                         'Я знаю уже {} пар(ы) слов, а ты нет.'.format(check_txt.add_separator(len(pairs))))
    except:
        error_message(message.chat.id)


@bot.message_handler(commands=['shutup'])
def shut_up(message):
    try:
        can_send = False
        admin_list = bot.get_chat_administrators(-1001457079924)
        for admin in admin_list:
            if message.from_user.id == admin.user.id:
                can_send = True
                break
        if not can_send:
            bot.send_message(message.chat.id, 'Я тебе этого не разрешаю, живи с этим.')
            return
        row = sqlunit.read_db(column_name='Name',
                              row_id='"silent"',
                              path='./misc/data.db',
                              table='service')
        if row[0][2] == '1':
            bot.send_message(message.chat.id, 'Молчу, молчу...')
            sqlunit.update_data(id_column='Name',
                                record='"silent"',
                                param_column='Value',
                                param_val='0',
                                table='service')
        elif row[0][2] == '0':
            sqlunit.update_data(id_column='Name',
                                record='"silent"',
                                param_column='Value',
                                param_val='1',
                                table='service')
            bot.send_message(message.chat.id, 'А поговорить?')
    except:
        error_message(message.chat.id)


@bot.message_handler(commands=['gender'])
def get_gender(message):
    try:
        bot.send_message(message.chat.id, gender.get_gender())
        achievements.update_auto_ach(20, message.from_user.id, message, bot)
    except:
        error_message(message.chat.id)


@bot.message_handler(commands=['myach'])
def my_ach(message):
    try:
        a = achievements.view_achievements(message.from_user.id)
        if a:
            media = []
            for file in a:
                f = open(file, 'rb')
                media.append(InputMediaPhoto(f))
            bot.send_media_group(message.chat.id, media)
            f.close()
        else:
            bot.send_message(message.chat.id,
                             achievements.ach_progress(message.from_user.id, message.from_user.username))
    except:
        error_message(message.chat.id)


@bot.message_handler(commands=['myprogress'])
def my_progress(message):
    try:
        a = achievements.view_achievements(message.from_user.id)
        if a:
            achievements.update_auto_ach(18, message.from_user.id, message, bot, make_null=True)
            achievements.update_auto_ach(18, message.from_user.id, message, bot, increment=random.randint(0, 999999))
            bot.send_message(message.chat.id,
                             achievements.ach_progress(message.from_user.id, message.from_user.username))
        else:
            bot.send_message(message.chat.id,
                             achievements.ach_progress(message.from_user.id, message.from_user.username))
    except:
        error_message(message.chat.id)


@bot.message_handler(commands=['delach'])
def del_ach(message):
    try:
        word, space, rest = message.text.partition(' ')
        if not rest.isdigit():
            bot.send_message(message.chat.id, 'Нужен номер ачивки, см. /myach')
            return
        a = achievements.del_achievment(message.from_user.id, rest)
        if not a:
            bot.send_message(message.chat.id, 'Нет такой ачивки, см. /myach')
        elif a == 1:
            bot.send_message(message.chat.id, 'Удолила!')
        elif a == -1:
            bot.send_message(message.chat.id, 'Не твоё — не трожь!')
    except:
        error_message(message.chat.id)


@bot.message_handler(commands=['summerinyoureyes'])
def summer(message):
    try:
        word, space, rest = message.text.partition(' ')
        user = sqlunit.read_db(column_name='Nick', row_id='"' + rest + '"')[0][0]
        if user:
            with open('./misc/main_message.pickle', 'rb') as f:
                message = pickle.load(f)
            achievements.update_auto_ach(29, int(user), message, bot)
        else:
            bot.send_message(message.chat.id, 'Косяк')
    except:
        error_message(message.chat.id)


@bot.message_handler(commands=['cookie'])
def cookie(message):
    try:
        achievements.cookie()
        bot.send_photo(message.chat.id, open('./misc/current_cookie.jpg', 'rb'))
        achievements.update_auto_ach(22, message.from_user.id, message, bot)
    except:
        error_message(message.chat.id)


@bot.message_handler(content_types=['text'])
def text(message):
    try:
        check_txt.check_txt(bot, message)
    except:
        error_message(message.chat.id)
