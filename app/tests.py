#!/usr/bin/python3.7

import unittest
import check_txt

import check_txt


class TextTest(unittest.TestCase):
    '''тесты текстовых процедур'''

    def test_reduplication(self):
        self.assertEqual('Хуяша!', check_txt.reduplication('Привет, Маша'))
        self.assertEqual('Хуёров!', check_txt.reduplication('Большой боров'))
        self.assertEqual('Хуюлица!', check_txt.reduplication('Улица'))
        self.assertEqual('Хуирка!', check_txt.reduplication('Бирка'))
        self.assertEqual('Хуемпл!', check_txt.reduplication('Сэмпл'))
        self.assertEqual('Хуятка!', check_txt.reduplication('Река      Вятка'))
        self.assertEqual('Хуёлка!', check_txt.reduplication('Высокая\nёлка'))
        self.assertEqual('Хуюла!', check_txt.reduplication('Юла'))
        self.assertEqual('Хуинда!', check_txt.reduplication('Тында'))
        self.assertEqual('Хуерег!', check_txt.reduplication('Берег'))
        self.assertEqual('Сам попробуй срифмуй, маяковский!', check_txt.reduplication('ЖЖЖЖЖ'))
        self.assertEqual('Сам попробуй срифмуй, маяковский!', check_txt.reduplication('123456789'))


if __name__ == '__main__':
    unittest.main()
