from PIL import Image
from PIL import ImageFont
from PIL import ImageDraw

def generate_text(text):
    font = ImageFont.truetype('./achievements/arial.ttf', 40)
    img = Image.new('1', font.getsize(text))
    mask = [x for x in font.getmask(text, mode='1')]
    img.putdata(mask)
    img = img.convert('RGB')
    return img

def add_border(img, width):
    new = Image.new('RGB', (img.width + 2 * width, img.height), (0, 0, 0))
    new.paste(img, (width, 0))
    return new

def shear(img, shear):
    img = img.convert("RGBA")
    shear = img.transform(img.size, Image.AFFINE, (1, shear, 0, 0, 1, 0))
    pixdata = shear.load()
    width, height = img.size
    for y in range(height):
        for x in range(width):
            if pixdata[x, y] == (0, 0, 0, 255):
                pixdata[x, y] = (255, 255, 255, 0)
    return shear

img = generate_text(' A')
img = add_border(img, 30)
img = shear(img, -1)
img.save('./misc/test.png', 'PNG')


# f len(text) % 2:
#             if len(text) <= 7:
#                 factor = 0.0005
#             elif len(text) > 25:
#                 factor = 0.00005
#             else:
#                 factor = ((0.0005 - 0.00005) / 18) * (25 - len(text)) + 0.00005
#             current_h = 225 - int(-(factor * (float(n) - (len(text) // 2)) ** 2) * 5000)