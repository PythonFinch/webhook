from pymongo import MongoClient
import sqlunit
import datetime
from pprint import pprint


#from misc import mongo_string


def create_users_table():

    users = sqlunit.read_db()

    first = users[0]
    print(first)

    client = MongoClient(mongo_string)

    client = MongoClient('localhost', 27017)


    db = client['masha_db']

    collection = db.users

    for user in users:
        user_to_insert = {'id': user[0],
                          'nick': user[1],
                          'real_name': user[2],
                          'is_pidor': bool(user[4]),
                          'month_pidor': user[5],
                          'words': user[7],

                          'achievements': []
                          }

        user_id = collection.insert_one(user_to_insert).inserted_id
        pprint(user_id)



def create_settings_collection():
    settings_db = sqlunit.read_db(table='service')

    client = MongoClient(mongo_string)

    db = client['masha_db']

    collection = db.settings

    for setting in settings_db:
        setting_to_insert = {'name': setting[1],
                             'value': setting[2],
                             }
        setting_id = collection.insert_one(setting_to_insert).inserted_id
        pprint(setting_id)


def create_auto_achs_collection():
    achs_db = sqlunit.read_db(table='hc_ach')

    client = MongoClient(mongo_string)

    db = client['masha_db']

    collection = db.auto_achievements

    for ach in achs_db:
        achs_progress = sqlunit.read_db(table='hc_ach_users', column_name='ach_id', row_id=ach[0])

        ach_to_insert = {'id': ach[0],
                         'name': ach[1],
                         'bronze': ach[2],
                         'gold': ach[3],
                         'platinum': ach[4],
                         'title': ach[5],
                         'body': ach[6],
                         'progress': [],
                         }

        array_ach = {}
        for i in achs_progress:
            if i[3] == ach[0]:
                array_ach['user_id'] = int(i[1])
                array_ach['nick'] = i[2]
                array_ach['value'] = i[5]
                array_ach['level'] = i[6]
                ach_to_insert['progress'].append(array_ach)
                array_ach = {}

        pprint(ach_to_insert)
        ach_id = collection.insert_one(ach_to_insert).inserted_id
        pprint(ach_id)

def read_db():
    client = MongoClient('localhost', 27017)
    db = client['masha_db']
    users = db.users
    for user in users.find():
        print(user)



def add_achievements():
    achievements = sqlunit.read_db(table='achievements')


    client = MongoClient(mongo_string)
    client = MongoClient('localhost', 27017)

    db = client['masha_db']

    collection = db.users

    ids = {}

    for achievement in achievements:
        try:
            ids[str(achievement[2])] += 1
        except KeyError:
            ids[str(achievement[2])] = 1
        print(achievement)
        ach_inner = {'id': ids[str(achievement[2])],
                     'from': int(achievement[1]),
                     'avatar': achievement[3],
                     'title': achievement[4],
                     'body': achievement[5],
                     'date': datetime.datetime.fromtimestamp(achievement[6]),
                     }
        # pprint(ach_inner)

        a = collection.update_one(
            {'id': int(achievement[2])},
            {
                '$push': {
                    'achievements': ach_inner

                }
            }
        )

        # print(a)


def drop_database():
    client = MongoClient(mongo_string)
    client.drop_database('masha_db')


def read_db(collection='users',
            field=None,
            value=None):
    try:
        client = MongoClient(mongo_string)
        db = client['masha_db']
        collection = db[collection]

        if not field:
            result = []
            for element in collection.find():
                result.append(element)
            return result
        else:
            result = collection.find({field: value})
            return result[0]
    except IndexError:
        return None


def add_data(data, collection='users'):
    client = MongoClient(mongo_string)
    db = client['masha_db']
    collection = db[collection]

    added_data = collection.insert_one(data).inserted_id

    return added_data


def update_data(data,
                field='id',
                value=None,
                collection='users'):
    client = MongoClient(mongo_string)
    db = client['masha_db']
    collection = db[collection]

    collection.update_one(
        {
            field: value,
        },
        {
            '$set': data
        }
    )


def edit_array(data,
               array,
               field='id',
               value=None,
               collection='users',
               action='add'):
    client = MongoClient(mongo_string)
    db = client['masha_db']
    collection = db[collection]

    if action == 'add':
        action = '$push'
    elif action == 'del':
        action = '$pull'

    '''pprint({
            field: value
        })
    pprint(
        {
            action: {
                array: data
            }
        })
    print('\n')'''

    collection.update_one(
        {
            field: value
        },
        {
            action: {
                array: data
            }
        }
    )


def find_auto_ach_value(user_id=None,
                        ach_id=None):
    client = MongoClient(mongo_string)
    db = client['masha_db']
    collection = db['auto_achievements']

    result = collection.find(
        {
            'id': ach_id,
        },
        {
            'bronze': 1,
            'gold': 1,
            'platinum': 1,
            'progress': 1
        }
    )

    # pprint(result[0]['progress'])
    result_row = next(item for item in result[0]['progress'] if item['user_id'] == user_id)
    result = {
        'bronze': result[0]['bronze'],
        'gold': result[0]['gold'],
        'platinum': result[0]['platinum'],
        'value': result_row['value'],
        'level': result_row['level']
    }
    return result


def add_auto_ach_value(user_id=None, ach_id=None, inc=1, field='value'):
    client = MongoClient(mongo_string)
    db = client['masha_db']
    collection = db['auto_achievements']

    collection.update_one(
        {
            'id': ach_id,
            'progress.user_id': user_id
        },
        {
            '$inc': {f'progress.$.{field}': inc}
        }
    )


if __name__ == '__main__':
    # drop_database()
    # create_users_collection()
    # create_settings_collection()
    # create_auto_achs_collection()
    # add_achievements()
    # row = read_db(collection='users',
    #                     field='id',
    #                     value=69934849)
    # print(row)
    # pprint(read_db())
    pass


    for achievement in achievements:
        print(achievement)
        ach_inner = {'id': int(achievement[0]),
                         'from': int(achievement[1]),
                         'avatar': achievement[3],
                         'title': achievement[4],
                         'body': achievement[5],
                         'date': datetime.datetime.fromtimestamp(achievement[6])
                         }
        ach_to_insert = {achievement[4]: ach_inner}
        pprint(ach_to_insert)



        a = collection.find_one_and_update(
            {'id': int(achievement[2])},
            {'$set':
                 {'achievements': ach_to_insert}
             }, upsert=True
        )

        print(a)


if __name__ == '__main__':
    # create_users_table()
    # read_db()
    add_achievements()

    pass
