#!/usr/bin/python3.7

from PIL import Image
from PIL import ImageFont
from PIL import ImageDraw

import os
import time
import sqlunit
from gender import create
import random
from misc import yk
import requests
from math import sin, cos, radians


class TextWrapper(object):
    """ Helper class to wrap text in lines, based on given text, font
        and max allowed line width.
    """

    def __init__(self, text, font, max_width):
        self.text = text
        self.text_lines = [
            ' '.join([w.strip() for w in l.split(' ') if w])
            for l in text.split('\n')
            if l
        ]
        self.font = font
        self.max_width = max_width

        self.draw = ImageDraw.Draw(
            Image.new(
                mode='RGB',
                size=(100, 100)
            )
        )

        self.space_width = self.draw.textsize(
            text=' ',
            font=self.font
        )[0]

    def get_text_width(self, text):
        return self.draw.textsize(
            text=text,
            font=self.font
        )[0]

    def wrapped_text(self):
        wrapped_lines = []
        buf = []
        buf_width = 0

        for line in self.text_lines:
            for word in line.split(' '):
                word_width = self.get_text_width(word)

                expected_width = word_width if not buf else \
                    buf_width + self.space_width + word_width

                if expected_width <= self.max_width:
                    # word fits in line
                    buf_width = expected_width
                    buf.append(word)
                else:
                    # word doesn't fit in line
                    wrapped_lines.append(' '.join(buf))
                    buf = [word]
                    buf_width = word_width

            if buf:
                wrapped_lines.append(' '.join(buf))
                buf = []
                buf_width = 0

        return '\n'.join(wrapped_lines)


def achievement(message, bot, whattosend=None, filename=None, background='bg.jpg'):
    try:
        time.sleep(2)
        if whattosend:
            message.caption = whattosend

        if message.caption.startswith('ачивка'):
            user = message.caption.split(' ')[1]
            word, space, rest = message.caption.partition(user)
            title = rest.split('"')
            title = [x for x in title if x and not x.isspace()]
            if not len(title) == 2 or not user.startswith('@'):
                raise ValueError
            auto_achs = sqlunit.read_db(path='./misc/data.db', table='hc_ach')
            for auto_ach in auto_achs:
                if auto_ach[5] == title[0] and background == 'bg.jpg':
                    raise ZeroDivisionError
    except ValueError:
        bot.send_message(message.chat.id, 'Формат клманды:\nачивка @кому "заголовок" "текст"')
        return
    except ZeroDivisionError:
        bot.send_message(message.chat.id, 'Неудачное имя ачивки!')
        return

    if not filename:
        fileID = message.photo[-1].file_id
        file_info = bot.get_file(fileID)
        downloaded_file = bot.download_file(file_info.file_path)

        with open("./achievements/tempimage.jpg", 'wb') as new_file:
            new_file.write(downloaded_file)

        im = Image.open("./achievements/tempimage.jpg")
    else:
        im = Image.open(filename)

    x = list(im.size)[0]
    y = list(im.size)[1]
    size = (400, 400)

    try:
        rows = sqlunit.read_db()
        usertoid = user
        for row in rows:
            if row[1] == user:
                usertoid = row[0]
        pathtosave = './achievements/{}'.format(usertoid)
        if not os.path.exists(pathtosave):
            os.makedirs(pathtosave)
        avaname = 'ava' + str(int(time.time()))
        if any((x < 400, y < 400)):
            box = (0, 0, min(x, y), min(x, y))
            region = im.crop(box)
            region = region.resize(size)
            region.save('{}/{}.jpg'.format(pathtosave, avaname), 'JPEG')
        else:
            box = (0, 0, 400, 400)
            region = im.crop(box)
            region.save('{}/{}.jpg'.format(pathtosave, avaname), 'JPEG')
    except IOError:
        bot.send_message(message.chat.id, "cannot create thumbnail for file")

    bg = Image.open('./achievements/{}'.format(background))
    box = (50, 50, 450, 450)
    bg.paste(region, box)

    draw = ImageDraw.Draw(bg)
    font1 = ImageFont.truetype('./achievements/impact.ttf', 60)
    font2 = ImageFont.truetype('./achievements/impact.ttf', 48)

    if background == 'bg_plat.jpg':
        fill = '#de5136'
    else:
        fill = 'white'

    wrapper = TextWrapper(title[0], font1, 900)
    wrapped_text = wrapper.wrapped_text().split('\n')[0]
    draw.text((550, 50), wrapped_text, font=font1, fill=fill)

    wrapper = TextWrapper(title[1], font2, 900)
    wrapped_text = wrapper.wrapped_text()
    draw.multiline_text((550, 150), wrapped_text, font=font2, spacing=3, fill=fill)

    bg.save('{}/ach{}.jpg'.format(pathtosave, avaname[3:]), 'JPEG')

    if background == 'bg.jpg':
        datatoadd = (None, str(message.from_user.id), str(usertoid), avaname, title[0], title[1], int(avaname[3:]))
    else:
        datatoadd = (None, '766336901', str(usertoid), avaname, title[0], title[1], int(time.time()))

    # if str(message.from_user.id) == str(usertoid):
    #    update_auto_ach(13, str(usertoid), message, bot, increment=1)

    update_auto_ach(21, message.from_user.id, message, bot, increment=1)

    sqlunit.add_data(datatoadd, path='./misc/data.db', table='achievements')
    bot.send_photo(message.chat.id, open('{}/ach{}.jpg'.format(pathtosave, avaname[3:]), 'rb'),
                   caption='{} получил(а) ачивку!'.format(user))


def view_achievements(id):
    achs = []
    rows = sqlunit.read_db(path='./misc/data.db', table='achievements')
    for row in rows:
        if str(row[2]) == str(id):
            achs.append((row[3], row[0]))
    if len(achs) == 0:
        return None

    pictures = [achs[x:x + 10] for x in range(0, len(achs), 10)]

    achs = []

    for n, pic in enumerate(pictures):
        if len(pic) < 9:
            box = (1500, 510 * len(pic) - 10)
        else:
            side = len(pic) // 2 + len(pic) % 2
            box = (3010, 500 * side + 10 * (side - 1))

        img = Image.new('RGB', box, color='white')

        for number, ach in enumerate(pic):
            if len(pic) < 9:
                x_pos = 0
                y_pos = number * 510
            else:
                x_pos = number % 2 * 1510
                y_pos = (number // 2) * 500 + (number // 2) * 10
                if number == len(pic) - 1 and len(pic) % 2 == 1:
                    x_pos = 750
            box = (x_pos, y_pos, x_pos + 1500, y_pos + 500)
            region = Image.open('./achievements/{}/ach{}.jpg'.format(id, ach[0][3:]))
            img.paste(region, box)

            draw = ImageDraw.Draw(img)
            font1 = ImageFont.truetype('./achievements/impact.ttf', 32)
            draw.text((x_pos + 5, y_pos + 5), str(ach[1]), (255, 255, 255), font=font1)

        img.save(f'./achievements/tempimage{n}.jpg', 'JPEG')
        achs.append(f'./achievements/tempimage{n}.jpg')
    return achs


def del_achievment(userid, achid):
    row = sqlunit.read_db(path='./misc/data.db', table='achievements', row_id=achid)
    if not row:
        return None

    if str(userid) == str(row[0][2]) or str(row[0][2]) == '766336901':
        sqlunit.del_data(path='./misc/data.db', table='achievements', id_column='id', record=str(achid))
        try:
            sqlunit.del_data(path='./misc/data.db', table='achievements', id_column='id', record=str(achid))
            os.remove('./achievements/{}/ach{}.jpg'.format(userid, row[0][3][3:]))
            os.remove('./achievements/{}/ava{}.jpg'.format(userid, row[0][3][3:]))
        except FileNotFoundError:
            return 1
        return 1
    else:
        return -1


def update_auto_ach(ach_id, user_id, message, bot, increment=1, make_null=False):
    ach_to_update = sqlunit.read_user_auto_ach(ach_id=ach_id,
                                               user_id=user_id,
                                               path='./misc/data.db',
                                               table='hc_ach_users')

    if not ach_to_update:
        return None

    new_value = ach_to_update[0][5] + increment
    if make_null:
        new_value = 0
    current_ach_id = ach_to_update[0][0]

    sqlunit.update_data(id_column='id',
                        record=current_ach_id,
                        param_column='Value',
                        param_val=new_value,
                        path='./misc/data.db',
                        table='hc_ach_users')

    level = ach_to_update[0][6]
    username = ach_to_update[0][2]

    achievement_data = sqlunit.read_db(column_name='id',
                                       row_id=ach_id,
                                       path='./misc/data.db',
                                       table='hc_ach')

    bronze, gold, platinum = achievement_data[0][2], achievement_data[0][3], achievement_data[0][4]
    title = achievement_data[0][5]
    body = achievement_data[0][6]
    filename = './achievements/#pre/' + achievement_data[0][1] + '.jpg'

    if level == 0 and new_value >= bronze:
        sqlunit.update_data(id_column='id',
                            record=current_ach_id,
                            param_column='Level',
                            param_val=1,
                            path='./misc/data.db',
                            table='hc_ach_users')
        achievement(message, bot,
                    whattosend='ачивка {} "{}" "{}"'.format(username, title, body),
                    filename=filename,
                    background='bg_bronze.jpg')
        level = 1

    if level == 1 and new_value >= gold:
        sqlunit.update_data(id_column='id',
                            record=current_ach_id,
                            param_column='Level',
                            param_val=2,
                            path='./misc/data.db',
                            table='hc_ach_users')

        user_ach_data = sqlunit.read_db(column_name='Title',
                                        row_id='"' + title + '"',
                                        path='./misc/data.db',
                                        table='achievements')

        row_to_del = None
        for row in user_ach_data:
            if str(row[2]) == str(user_id):
                row_to_del = row[0]

        if row_to_del:
            del_achievment(user_id, row_to_del)
        level = 2

        achievement(message, bot,
                    whattosend='ачивка {} "{}" "{}"'.format(username, title, body),
                    filename=filename,
                    background='bg_gold.jpg')

    if level == 2 and new_value >= platinum:
        sqlunit.update_data(id_column='id',
                            record=current_ach_id,
                            param_column='Level',
                            param_val=3,
                            path='./misc/data.db',
                            table='hc_ach_users')

        user_ach_data = sqlunit.read_db(column_name='Title',
                                        row_id='"' + title + '"',
                                        path='./misc/data.db',
                                        table='achievements')

        row_to_del = None
        for row in user_ach_data:
            if str(row[2]) == str(user_id):
                row_to_del = row[0]

        if row_to_del:
            del_achievment(user_id, row_to_del)

        achievement(message, bot,
                    whattosend='ачивка {} "{}" "{}"'.format(username, title, body),
                    filename=filename,
                    background='bg_plat.jpg')


def ach_progress(user_id, nick):
    auto_achs = sqlunit.read_db(path='./misc/data.db', table='hc_ach')
    user_achs = sqlunit.read_db(path='./misc/data.db',
                                table='hc_ach_users',
                                column_name='user_id',
                                row_id=user_id)  # iiiiiiiiiiiiiiiiii

    result = []

    for ach in auto_achs:
        number = ach[0]
        for user_ach in user_achs:
            if user_ach[3] == number:
                value = user_ach[5]
                name = ach[5]
                level = user_ach[6]
                bronze, gold, platinum = ach[2], ach[3], ach[4]
                if value < bronze:
                    percent = value / bronze * 100
                elif value in range(bronze, gold):
                    percent = (value - bronze) / (gold - bronze) * 100
                    if str(level) == '0':
                        percent += 100
                elif value in range(gold, platinum):
                    percent = (value - gold) / (platinum - gold) * 100
                    if str(level) == '1':
                        percent += 100
                result.append((name, round(percent)))
                break
    result.sort(key=lambda i: i[1], reverse=True)
    str_result = f'Вот твои ачивки, @{nick}!\n\n'
    for i in result:
        str_result += '{} | {}%\n'.format(i[0], i[1])
    return str_result


def cookie():
    URL = "https://translate.yandex.net/api/v1.5/tr.json/translate"
    KEY = yk

    cookies = []
    langs = ['en-uk', 'en-be', 'en-sr', 'en-bg', 'en-mk']
    create('./misc/cookies.txt', cookies)

    while True:
        random.seed()
        cookie = random.choice(cookies)
        params = {
            "key": KEY,
            "text": cookie,
            "lang": random.choice(langs)
        }
        response = requests.get(URL, params=params)
        cookie = response.json()["text"][0]
        print('результат', cookie)
        # cookie = translator.translate(cookie, dest=random.choice(langs)).text
        font = ImageFont.truetype('./misc/Monotype-Corsiva.TTF', 30)
        wrapper = TextWrapper(cookie, font, 370)
        cookie_lines = wrapper.wrapped_text().split(sep='\n')
        if len(cookie_lines) < 4:
            break

    image = Image.open('./misc/cookie.jpg')
    draw = ImageDraw.Draw(image)

    current_h = 350 - (len(cookie_lines) - 1) * 15
    pad = 6

    for line in cookie_lines:
        w, h = draw.textsize(line, font=font)
        draw.text(((980 - w) / 2, current_h), line, font=font, fill='black')
        current_h += h + pad

    image.save('./misc/current_cookie.jpg', 'JPEG')


def advice(text):
    font = ImageFont.truetype('./achievements/impact.ttf', 50)
    wrapper = TextWrapper(text, font, 500)
    text_lines = wrapper.wrapped_text().split(sep='\n')

    image = Image.open('./misc/advice.jpg')
    draw = ImageDraw.Draw(image)
    shadowcolor = 'black'

    current_h = 225 - (len(text_lines) - 1) * 15
    pad = 7
    border = 2

    for line in text_lines:
        w, h = draw.textsize(line, font=font)
        draw.text(((680 - w) / 2 - border, current_h - border), line, font=font, fill=shadowcolor)
        draw.text(((680 - w) / 2 + border, current_h - border), line, font=font, fill=shadowcolor)
        draw.text(((680 - w) / 2 - border, current_h - border), line, font=font, fill=shadowcolor)
        draw.text(((680 - w) / 2 + border, current_h + border), line, font=font, fill=shadowcolor)
        draw.text(((680 - w) / 2, current_h), line, font=font, fill='white')
        current_h += h + pad

    image.save('./misc/current_advice.jpg', 'JPEG')

    return './misc/current_advice.jpg'


def add_auto_achievement(achievement):
    from pprint import pprint
    pidorlist = sqlunit.pidors_list()
    pprint(pidorlist)

    sqlunit.add_data(achievement, table='hc_ach')

    current_achievements = sqlunit.read_db(table='hc_ach')

    for i in pidorlist:
        data_to_add = (None, i[0], i[1], current_achievements[-1][0], achievement[1], 0, 0)
        sqlunit.add_data(data_to_add, table='hc_ach_users')


def lower_turtles(text, dimensions=None):
    text = text.upper()
    font = ImageFont.truetype('./achievements/TMNT.ttf', 140)
    width = font.getsize(text)[0]
    if not dimensions:
        image = Image.open('./misc/turtles2.jpg')
    else:
        image = Image.new('RGBA', dimensions, (255, 0, 0, 0))
    draw = ImageDraw.Draw(image)

    color = (156, 203, 64)
    shadow = 'black'
    max_x, max_y = 0, 0
    coords = []
    max_bottom = 0

    for n, letter in enumerate(text):
        # вычисление координат буквы
        radius = width
        coord_angle = 250 + n * (40 / (len(text) - 1))

        coord_x = cos(radians(coord_angle)) * radius
        coord_y = sin(radians(coord_angle)) * radius

        max_x = max(max_x, abs(coord_x))
        max_y = max(max_y, abs(coord_y))

        coords.append((coord_x, coord_y))

    for n, letter in enumerate(text):
        w, h = draw.textsize(letter, font=font)

        letter_image = Image.new('RGBA', (w, h))
        shadow_image = Image.new('RGBA', (w, h))

        main_letter = ImageDraw.Draw(letter_image)
        main_letter.text((0, 0), letter, font=font, fill=color)
        shadow_letter = ImageDraw.Draw(shadow_image)
        shadow_letter.text((0, 0), letter, font=font, fill=shadow)

        coord_angle = 250 + n * (40 / (len(text) - 1))

        if len(text) % 2:
            # вычисление угла поворота буквы
            angle = -(coord_angle + 90)
        else:
            if n == len(text) // 2 or n == len(text) // 2 - 1:
                angle = 0
            else:
                angle = -(coord_angle + 90)

        letter_x = int(coords[n][0] + max_x)
        letter_y = int(coords[n][1] + max_y)

        rotated_letter = letter_image.rotate(angle, expand=1)
        rotated_shadow = shadow_image.rotate(angle, expand=1)
        if n == 0:
            most_left = letter_x - 2
            most_bottom = max(max_bottom, letter_y + rotated_letter.size[1] + 2)
        elif n == len(text) - 1:
            most_right = letter_x + rotated_letter.size[0] + 2
            most_bottom = max(max_bottom, letter_y + rotated_letter.size[1] + 2)
        elif n == len(text) // 2:
            most_above = letter_y + 2

        border = 7

        image.paste(rotated_shadow, (letter_x - border, letter_y - border), rotated_shadow)
        image.paste(rotated_shadow, (letter_x - border, letter_y + border), rotated_shadow)
        image.paste(rotated_shadow, (letter_x + border, letter_y - border), rotated_shadow)
        image.paste(rotated_shadow, (letter_x + border, letter_y + border), rotated_shadow)

        image.paste(rotated_letter, (letter_x, letter_y), rotated_letter)
    if not dimensions:
        lower_turtles(text, (most_right - most_left, most_bottom - most_above + 10))
    else:
        image.save('./misc/bottom.png', 'PNG')


def upper_turtles(text):
    text = text.upper()
    font = ImageFont.truetype('./achievements/arial.ttf', 40)

    img_left = Image.open('./misc/left.png').convert("RGBA")
    img_right = Image.open('./misc/right.png').convert("RGBA")

    current_h = 0
    current_w = 0

    width = font.getsize(text)[0]
    value = (width / 1000) ** (1 / 8)
    width = int(width * value)
    image = Image.new('RGBA', (62 + width + 62, 65), (255, 0, 0, 0))
    draw = ImageDraw.Draw(image)

    image.paste(img_left, (current_w, current_h), img_left)
    image.paste(img_right, (current_w + 62 + width, current_h), img_right)
    draw.rectangle(((62, 0), (62 + width) + 1, 7), fill="black")
    draw.rectangle(((62, 8), (62 + width + 1, 64)), fill=(254, 0, 0))
    draw.rectangle(((62, 57), (62 + width + 1, 65)), fill="black")

    current_w += 30

    for n, letter in enumerate(text):
        if letter == 'А' or letter == 'A':
            img = generate_text(' ' + 'А')
            w_diff = -font.getsize(' ')[0]
        else:
            img = generate_text(letter)
            w_diff = 0
        img = add_border(img, 10)
        shear_value = -0.5 + n * (1 / (len(text) - 1))
        img = shear(img, shear_value)

        width_of_letter = font.getsize(letter)
        if letter.upper() == 'Ё' or letter.upper() == 'Й':
            h_diff = 10
        else:
            h_diff = 17

        image.paste(img, (current_w + w_diff, current_h + h_diff), img)
        current_w += width_of_letter[0]

    image.save('./misc/top.png', 'PNG')


def generate_text(text):
    font = ImageFont.truetype('./achievements/arial.ttf', 40)
    img = Image.new('1', font.getsize(text))
    mask = [x for x in font.getmask(text, mode='1')]
    img.putdata(mask)
    img = img.convert('RGB')
    return img


def add_border(img, width):
    new = Image.new('RGB', (img.width + 2 * width, img.height), (0, 0, 0))
    new.paste(img, (width, 0))
    return new


def shear(img, shear):
    img = img.convert("RGBA")
    shear = img.transform(img.size, Image.AFFINE, (1, shear, 0, 0, 1, 0))
    pixdata = shear.load()
    width, height = img.size
    for y in range(height):
        for x in range(width):
            if pixdata[x, y] == (0, 0, 0, 255):
                pixdata[x, y] = (255, 255, 255, 0)
    return shear


def combine():
    top_img = Image.open('./misc/top.png').convert("RGBA")
    bottom_img = Image.open('./misc/bottom.png').convert("RGBA")
    new_image_dimensions = (max(top_img.size[0], bottom_img.size[0]), 65 + bottom_img.size[1] - 40)
    final_img = Image.new('RGBA', new_image_dimensions)

    if top_img.size[0] > bottom_img.size[0]:
        final_img.paste(top_img, (0, 0), top_img)
        final_img.paste(bottom_img, (int((top_img.size[0] - bottom_img.size[0]) / 2), 25), bottom_img)
    elif top_img.size[0] < bottom_img.size[0]:
        final_img.paste(top_img, (int((bottom_img.size[0] - top_img.size[0]) / 2), 0), top_img)
        final_img.paste(bottom_img, (0, 25), bottom_img)
    else:
        final_img.paste(top_img, (0, 0), top_img)
        final_img.paste(bottom_img, (0, 25), bottom_img)

    final_img.save('./misc/final.png', 'PNG')

