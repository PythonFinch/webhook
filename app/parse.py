import pickle
import requests
from bs4 import BeautifulSoup
from multiprocessing import Pool
from requests.adapters import HTTPAdapter
from requests.packages.urllib3.util.retry import Retry
import time
from copy import deepcopy
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.common import exceptions
from selenium.common.exceptions import NoSuchElementException, StaleElementReferenceException, WebDriverException


def get_html(url):
    session = requests.Session()
    retry = Retry(connect=3, backoff_factor=0.5)
    adapter = HTTPAdapter(max_retries=retry)
    session.mount('http://', adapter)
    session.mount('https://', adapter)

    r = session.get(url)
    return r.text


def parse(i):
    """Main parse function"""
    try:
        with open('anekrunums.pickle', 'rb') as file:
            numbers = pickle.load(file)
        if i in numbers:
            print(i, ' :PASS')
            return
    except pickle.UnpicklingError:
        print('Unpicling error!')
        return
    except EOFError:
        print('EOF error!')
        return

    try:
        # url = 'https://www.anekdot.ru/id/{}'.format(i)
        url = 'http://anekdot.me/wiki/{}'.format(i)
        '''profile = webdriver.FirefoxProfile()
        # 1 - Allow all images
        # 2 - Block all images
        # 3 - Block 3rd party images
        profile.set_preference("permissions.default.image", 2)

        driver = webdriver.Firefox(firefox_profile=profile)
        driver.get(url)
        anekdot = driver.find_element(By.XPATH, '//div[@class="text"]').text
        driver.close()'''

        html = get_html(url)
        soup = BeautifulSoup(html, 'lxml')
    except requests.exceptions.ConnectionError:
        print('Connection error!')
        time.sleep(5)
    except UnicodeDecodeError:
        print('Unicode error!')
        return

    try:
        letter = soup.find('font', color='#FFAE00').get_text().strip()
        text = letter + soup.find('font',
                                  style='font-family:Open Sans,Tahoma,Verdana').get_text().strip()
        if True:

            '''with open('anekrunums.pickle', 'rb') as file:
                numbers = pickle.load(file)
            numbers.add(i)
            with open('anekrunums.pickle', 'wb') as f:
                pickle.dump(numbers, f)'''

            '''with open('anekru.txt', 'a', encoding='utf-8') as f:
                f.write(text)
                f.write('\n==========\n')'''

            try:
                a = deepcopy(text)
                a.encode('utf-8')
            except UnicodeDecodeError:
                print('Unicode error!')
                return
            with open('anekrunewanek.pickle', 'rb') as f:
                newanek = pickle.load(f)
            newanek.add(text)
            with open('anekrunewanek.pickle', 'wb') as f:
                pickle.dump(newanek, f)

            print(str(i) + '. ' + text)

    except AttributeError:
        print('AttrError!')
    except FileNotFoundError:
        print('File not found error!')
    except pickle.UnpicklingError:
        print('Unpicling error!')
    except EOFError:
        print('EOF error!')
    except WebDriverException:
        print('Selenium exception!')
        driver.close()


def create_pickle():
    with open('anekruewanek.pickle', 'rb') as f:
        newanek = pickle.load(f)
        print('len of new = ', len(newanek))

    with open('anek.pickle', 'rb') as f:
        oldanek = pickle.load(f)

    oldanek = set(oldanek)

    print('old len = ', len(oldanek))

    oldanek.update(newanek)

    print('sum len = ', len(oldanek))

    with open('anek.pickle', 'wb') as f:
        pickle.dump(oldanek, f)


def txttopickle(filename, sep):
    with open(filename, 'r', encoding='utf-8') as f:
        text = f.read()

    listnew = text.split(sep)
    listnew = [x.strip() for x in listnew]
    print('Новых анекдотов: ', len(set(listnew)))

    with open('anek.pickle', 'rb') as f:
        oldanek = pickle.load(f)

    oldanek = set(oldanek)

    print('Старых = ', len(oldanek))

    oldanek.update(set(listnew))

    print('sum len = ', len(oldanek))

    with open('anek.pickle', 'wb') as f:
        pickle.dump(oldanek, f)


def make_all(start, finish):
    try:
        with open('anekrunums.pickle', 'rb') as f:
            nums = pickle.load(f)
        with open('anekrunewanek.pickle', 'rb') as f:
            newanek = pickle.load(f)
    except FileNotFoundError:
        emptyset = set()
        with open('anekrunums.pickle', 'wb') as f:
            nums = pickle.dump(emptyset, f)
        with open('anekrunewanek.pickle', 'wb') as f:
            nums = pickle.dump(emptyset, f)

    if nums:
        oldlengthnums = len(nums)
        print('len of nums = ' + str(len(nums)))
    else:
        oldlengthnums = 0

    lst = [str(i) for i in range(start, finish)]

    while True:
        with Pool(10) as p:
            p.map(parse, lst)

        with open('anekrunums.pickle', 'rb') as f:
            newnums = pickle.load(f)
        newlengthnums = len(newnums)
        if oldlengthnums == newlengthnums:
            break
        else:
            oldlengthnums = newlengthnums

    with open('anekrunewanek.pickle', 'rb') as f:
        newanek = pickle.load(f)

    print(len(newanek))


def get_proxies_list():
    proxies = []
    url = 'https://free-proxy-list.net/'
    html = get_html(url)
    print(html)
    soup = BeautifulSoup(html, 'lxml')
    rows = soup.find_all('tr', role='row')
    print(rows)

    for row in rows[1:]:
        ip = row.text.split()[0]
        port = row.text.split()[1]
        proxies.append(ip + ':' + port)
    print(proxies)
    return proxies


if __name__ == '__main__':
    # get_proxies_list()
    # make_all(0, 100)
    # create_pickle()
    #txttopickle('Moskal_A-sho-Anekdoty-pro-hohlov.436063.fb2.txt', '\n♦ ♦ ♦\n\n')
    with open('./misc/anek.pickle', 'rb') as f:
        aneks = pickle.load(f)
        aneks = list(aneks)
        print(len(aneks))
        for element in aneks:
            if len(element) < 20 or len(element) > 1500 or 'http' in element:
                aneks.remove(element)
        print(len(aneks))
        aneks = set(aneks)
        with open('./misc/anek.pickle', 'wb') as f:
            pickle.dump(aneks, f)



