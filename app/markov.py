#!/usr/bin/env python

import markovify
import random
import pymorphy2
import pickle
from string import punctuation


def get_shitpost(message):
    morph = pymorphy2.MorphAnalyzer()
    HISTORY_FILENAME = "./misc/history.txt"
    SOROKIN_FILENAME = "./misc/sorokin.txt"

    # Get raw text as string.
    with open(HISTORY_FILENAME, encoding="utf8") as f:
        text1 = f.read()
    with open(SOROKIN_FILENAME, encoding="utf8") as f:
        text2 = f.read()
    with open('./misc/emoji.pickle', 'rb') as f:
        emojis = pickle.load(f)

    # Build the model.
    text_model1 = markovify.Text(text1, state_size=2)
    text_model2 = markovify.Text(text2, state_size=2)

    model_combo = markovify.combine([ text_model1, text_model2 ], [ 1, 1 ])

    words_dict = []
    clearmessage = "".join(i for i in message if i not in punctuation)
    clearmessage = clearmessage.replace('\n', ' ')
    message_as_list = clearmessage.split(' ')
    message_lower = list(set([x.lower() for x in message_as_list]))
    for word in message_lower:
        words_dict.append(word)
        for parse in morph.parse(word):
            for lexeme in parse.lexeme:
                words_dict.append(lexeme.word)

    words_dict = list(set(words_dict + [x.upper() for x in words_dict] + [x.capitalize() for x in words_dict] + [x.lower() for x in words_dict]))
    random.shuffle(words_dict)
    result = None
    for word in words_dict:
        try:
            result = model_combo.make_sentence_with_start(word, tries=1000)
        except KeyError:
            continue
        if result:
            break
    if not result:
        result = model_combo.make_sentence(tries=1000)

    result += ' '

    emojiwall = random.randint(1, 1000) == 777
    if emojiwall:
        for i in range(0, random.randint(100, 200)):
            result += random.choice(emojis)

    for i in range(0, random.randint(0, 3)):
        result += random.choice(emojis)

    return result