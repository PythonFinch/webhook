#!/usr/bin/python3.7

import re
import random
import gender
from bs4 import BeautifulSoup
import requests
import sqlunit
import sqlite3
import markov
import locale
import achievements
import datetime
from telebot.apihelper import ApiException
from monopoly import random_event

lama_code = 'CAADAgADFwADAR8rBK7IYUopSgODFgQ'
nahuj_code = 'CAADAgADFgADAR8rBOwu1vhY8pnOFgQ'
bot_id = '766336901'

max_coin = 20000


def reduplication(text, message, bot):
    if not text.lower().startswith('маш') and not ' маш' in text.lower():
        return None
    vowels = {'а': 'я',
              'о': 'ё',
              'у': 'ю',
              'ы': 'и',
              'э': 'е'}

    without_symbols = re.sub(r"[^\w]*$", '', text)
    without_symbols = without_symbols.replace('\n', ' ')
    last_word = without_symbols.split(' ')[-1].lower()

    ind = 0
    for index, char in enumerate(last_word):
        if char in vowels.keys() or char in vowels.values():
            ind = index
            break

    if last_word[ind] in vowels.values():
        result = 'Ху' + last_word[ind:] + '!'
    elif last_word[ind] in vowels.keys():
        iotated_vowel = vowels.get(last_word[ind])
        result = 'Ху' + iotated_vowel + last_word[ind + 1:] + '!'
    else:
        result = 'Сам попробуй срифмуй, маяковский!'
    return result


def advice(text, message, bot):
    if random.randint(1, 99) > 33:
        verbs = []
        gender.create('./misc/verbs.txt', verbs)
        text = 'Ой, да просто {}, и всё))) Сначала {}, потом {}, чё ты как ребёнок)))'.format(random.choice(verbs),
                                                                                              random.choice(verbs),
                                                                                              random.choice(verbs))
        advice_img = achievements.advice(text)
        achievements.update_auto_ach(17, message.from_user.id, message, bot, increment=1)
        return 'image:' + advice_img
    else:
        achievements.update_auto_ach(8, message.from_user.id, message, bot)
        return 'sticker:' + nahuj_code


def wiki_dict(text, message, bot):
    word_to_find = re.sub(r"[^\w]*$", '', text[text.find('что такое') + 10:])
    url = 'https://ru.wiktionary.org/w/index.php?title={}&action=raw'
    try:
        page = requests.get(url.format(word_to_find.lower()))
        soup = BeautifulSoup(page.text, 'lxml')
        raw = soup.find('body').text
        result = re.search(r'==== Значение ====(.+?)====', raw, flags=re.S)
        definition = result.group(1)
        definition = re.sub('\|[a-z]{2}', '', definition)
        definition = re.sub('[а-яА-Я]*\|', '', definition)
        definition = re.sub(r'(# \n)|(#\n)|(источник=[а-яА-Я0-9]*)', '', definition)
        definition = re.sub(r'(\[\[)|(\]\])|({{)|(}})', '', definition)
        definition = definition.replace('\n', '\n\n')
        definition.encode("utf-8").decode("utf-8")
        return '<b>{}</b> \n {}'.format(word_to_find.capitalize(), definition.strip()[:3000])
    except AttributeError:
        achievements.update_auto_ach(8, message.from_user.id, message, bot)
        return 'sticker:' + nahuj_code


def course(text, message, bot):
    prev_text = (' нынче по ', ' на ближайшем рынке отдают по ', ', если верить ЦБ, ',
                 ' в базарный день ', ' ты всё равно не можешь себе позволить купить по ')

    if 'USD' in text and 'MOON' not in text:
        return 'Долоры{}{}'.format(random.choice(prev_text), get_currency('USD'))

    elif 'EUR' in text and 'MOON' not in text:
        return 'Ойро{}{}'.format(random.choice(prev_text), get_currency('EUR'))

    elif 'USD' in text and 'MOON' in text:
        return 'За один доллар ты сможешь купить {} лун(ы).'.format(moon_course(1))

    elif 'EUR' in text and 'MOON' in text:
        return 'За один евро ты сможешь купить {} лун(ы).'.format(moon_course(1.12781954887218))


def hitler(text, message, bot):
    return 'sticker:' + lama_code


def rhyme(text, message, bot):
    pattern = r'.*((ас)|(аз))\W*\d*$'
    if re.fullmatch(pattern, text.lower()):
        return 'sticker:' + random.choice(('CAADAgADHwADAR8rBLsb7KxnXIdrFgQ', 'CAADAgADIAADAR8rBIE0OshX16vPFgQ'))


def yes_or_no(text, message, bot):
    text = re.sub(r"[^\w]*$", '', text)
    if text.upper() == ('ПИЗДА'):
        return 'Да'

    elif text.upper() == ('ДА'):
        return 'Пизда'

    elif text.upper() == ('НЕТ'):
        return 'Пидора ответ'


def not_great(text, message, bot):
    return 'Not great, not terrible'


def russians_forward(text, message, bot):
    result = '<b>'
    for i in range(1, random.randint(2, 10)):
        result += 'РУССКИЕ ВПЕРЁД!\n'
    result += '</b>'
    return result


def russians_forward2(text, message, bot):
    pattern = r'.*((ёт)|(ёд))\W*\d*$'
    if re.fullmatch(pattern, text.lower()):
        return russians_forward(text, message, bot)


def toad(text, message, bot):
    achievements.update_auto_ach(31, message.from_user.id, message, bot, increment=1)
    return 'media:./misc/toads.mp4'


def check_txt(bot, message):
    will_shitpost = True
    function_dict = {
        'дай совет': advice,
        'что такое': wiki_dict,
        'русски': russians_forward,
        'ёт': russians_forward2,
        'ёд': russians_forward2,
        'маш': reduplication,
        'USD': course,
        'EUR': course,
        'MOON': course,
        'гитлер': hitler,
        'hitler': hitler,
        'хитлер': hitler,
        'ас': rhyme,
        'аз': rhyme,
        'да': yes_or_no,
        'пизда': yes_or_no,
        'нет': yes_or_no,
        '3,6': not_great,
        '3.6': not_great,
        'жаб': toad,
    }
    post = None
    for key in function_dict:
        if key.lower() in message.text.lower():
            post = function_dict.get(key)(message.text, message, bot)
            break

    try:
        if post:
            will_shitpost = False
            if post.startswith('sticker'):
                bot.send_sticker(message.chat.id, post.split(':')[1])
            elif post.startswith('image:'):
                bot.send_photo(message.chat.id, open(post.split(':')[1], 'rb'))
            elif post.startswith('media:'):
                bot.send_document(message.chat.id, open(post.split(':')[1], 'rb'))
            else:
                bot.send_message(message.chat.id, post, parse_mode="HTML")
        else:
            if datetime.datetime.today().weekday() == 2:
                row = sqlunit.read_db(column_name='id',
                                      row_id=8,
                                      path='./misc/data.db',
                                      table='service')
                if int(row[0][2]) < 5:
                    if datetime.datetime.now().hour > 5 and random.randint(datetime.datetime.now().hour, 24) > 22:
                        sqlunit.update_data(id_column='id',
                                            record=8,
                                            param_column='"Value"',
                                            param_val=str(int(row[0][2]) + 1),
                                            table='service')
                        bot.send_photo(message.chat.id, open('./misc/wednesday.jpg', 'rb'))

    except ApiException:
        bot.send_sticker(message.chat.id, nahuj_code)

    # часть tmnt
    vowels = {'а': 'я',
              'о': 'ё',
              'у': 'ю',
              'ы': 'и',
              'э': 'е'}
    vowel_count = 0
    without_symbols = ''
    for letter in message.text:
        if letter in vowels.keys() or letter in vowels.values():
            vowel_count += 1
        if letter.isalpha() or letter.isdigit() or letter.isspace():
            without_symbols += letter
    if vowel_count == 8:
        print(without_symbols)
        lower_text = without_symbols.split(' ')[-1].strip()
        upper_text = without_symbols[0:-len(lower_text)-1].strip()
        achievements.upper_turtles(upper_text)
        achievements.lower_turtles(lower_text)
        achievements.combine()
        bot.send_photo(message.chat.id, open('./misc/final.png', 'rb'))

    row = sqlunit.read_db(column_name='id',
                          row_id=10,
                          path='./misc/data.db',
                          table='service')

    probability = (float(row[0][2]) - 60)
    if '🌚' in message.text or '🌝' in message.text:
        probability *= 2
    if random.randint(0, 1000) < probability and message.chat.id == -1001457079924:
        random_event(message, bot)
        will_shitpost = False

    many_words, percent, current_moon, max_moon = edit_sql_data(message, bot)

    if random.randint(1, 10) < 6 and current_moon:
        will_shitpost = False
        bot.send_message(message.chat.id,
                         'Твой лунобаланс {} шт., а максимум лун {} итд.'.format(add_separator(current_moon),
                                                                                 add_separator(max_moon)))
    if moon_bank() > max_coin:
        achievements.update_auto_ach(5, message.from_user.id, message, bot, increment=1)
        inflation()
        will_shitpost = False
        bot.send_message(message.chat.id,
                         'Доигрались! Луны обесценились! Дефолт! МММ пизда! Ельцин защити! ААААААААААААААААА')

    text_to_write = clear_text(message.text)
    if text_to_write:
        with open('./misc/history.txt', 'a', encoding='utf8') as file:
            file.write(text_to_write)

    if will_shitpost:
        if (message.reply_to_message and
            message.reply_to_message.from_user.id == int(bot_id)) or \
                random.randint(1, 100) < percent:
            if (message.reply_to_message and
                    message.reply_to_message.from_user.id == int(bot_id)):
                achievements.update_auto_ach(30, message.from_user.id, message, bot, increment=1)

            result = markov.get_shitpost(message.text)
            if many_words != 0:
                while len(result) < many_words * 100:
                    result = result + ' ' + markov.get_shitpost(message.text)

            bot.send_message(message.chat.id, result)

    leprecon = random.randint(0, 1000000000)
    if leprecon == 7:
        achievements.update_auto_ach(3, message.from_user.id, message, bot, increment=1)

    if 'хуй' in message.text \
            or 'пизд' in message.text \
            or 'бля' in message.text \
            or 'еба' in message.text \
            or 'ёба' in message.text \
            or 'хуе' in message.text \
            or 'хуи' in message.text \
            or 'хую' in message.text \
            or 'хуя' in message.text \
            or 'хую' in message.text \
            or 'хуё' in message.text:
        achievements.update_auto_ach(19, message.from_user.id, message, bot, increment=1)


def get_currency(currency):
    roubles = ('деревянных', 'ьублей', 'рупиев', 'рублей', 'пошёл нахуй',
               'злых русских бумажек', 'рашн роублес')

    url = 'https://www.cbr-xml-daily.ru/daily_json.js'
    response = requests.get(url).json()
    price = response['Valute']['{}'.format(currency)]['Value']
    return str(price) + ' ' + random.choice(roubles)


def moon_course(factor):
    row = sqlunit.read_db(column_name='id',
                          row_id=1,
                          path='./misc/data.db',
                          table='service')
    m_course = int(row[0][2]) ** (moon_bank() * 256 / max_coin) * factor
    return add_separator(m_course)


def edit_sql_data(message, bot):
    many_words, percent, current_moon, max_moon = None, None, None, None
    if message.from_user.username:
        nick = '@' + message.from_user.username
    else:
        nick = 'addusernameplz'
    words = message.text.split()
    word_count = len(words)

    row = sqlunit.read_db(row_id=message.from_user.id)
    if row:
        new_name = ''
        if message.from_user.first_name:
            new_name += str(message.from_user.first_name)
        if message.from_user.last_name:
            new_name = new_name + ' ' + str(message.from_user.last_name)
        if new_name == '':
            new_name = nick
        if new_name != row[0][2]:
            sqlunit.update_data(id_column='id',
                                record=int(message.from_user.id),
                                param_column='RealName',
                                param_val=new_name)
        if nick != row[0][1]:
            sqlunit.update_data(id_column='id',
                                record=int(message.from_user.id),
                                param_column='Nick',
                                param_val=nick)

        word_total_count = word_count + int(row[0][8])
        word_count += int(row[0][7])

        sqlunit.update_data(id_column='id',
                            record=int(message.from_user.id),
                            param_column='WordsMonth',
                            param_val=word_count)
        sqlunit.update_data(id_column='id',
                            record=int(message.from_user.id),
                            param_column='WordsTotal',
                            param_val=word_total_count)
        sqlunit.update_data(id_column='id',
                            record=int(message.from_user.id),
                            param_column='LastMessage',
                            param_val=int(datetime.datetime.now().timestamp()))

        achievements.update_auto_ach(1, message.from_user.id, message, bot, increment=len(words))

        row = sqlunit.read_db(table='service')
        if row[3][2] != message.chat.title and message.chat.id == -1001457079924:
            sqlunit.update_data(id_column='id',
                                record=4,
                                param_column='"Value"',
                                param_val=message.chat.title,
                                table='service')
        many_words = int(row[4][2])  # как много писать
        percent = int(row[5][2])  # процент выпадения ответов

    else:
        new_member = [0 for x in range(0, 10)]
        new_member[0], new_member[1], new_member[2] = \
            int(message.from_user.id), nick, str(message.from_user.first_name)

        new_member[7], new_member[8] = word_count, word_count
        sqlunit.add_data(new_member)

    if '🌚' in message.text or '🌝' in message.text:
        count = message.text.count('🌚') + message.text.count('🌝')

        max_moon = sqlunit.max('Moons')[0][0]
        row = sqlunit.read_db(row_id=message.from_user.id)
        current_moon = int(row[0][3]) + count
        sqlunit.update_data(id_column='id',
                            record=row[0][0],
                            param_column='Moons',
                            param_val=current_moon)
        max_moon = max(max_moon, int(row[0][3]), current_moon)

        achievements.update_auto_ach(2, message.from_user.id, message, bot, increment=count)

        if count > 50:
            random_event(message, bot, exact=0)

    return many_words, percent, current_moon, max_moon


def inflation():
    try:
        with sqlite3.connect('./misc/data.db') as db:
            cursor = db.cursor()
            cursor.execute('SELECT id, Moons FROM users')
            ids = cursor.fetchall()
            for idd, moons in ids:
                sqlunit.update_data(id_column='id',
                                    record=idd,
                                    param_column='Moons',
                                    param_val=int(moons / 1000))

        sqlunit.del_data(id_column='Title',
                         record='Moon Pie',
                         path='./misc/data.db',
                         table='achievements')
        sqlunit.update_data(id_column='ach_name',
                            record='Moons',
                            param_column='Value',
                            param_val='0',
                            path='./misc/data.db',
                            table='hc_ach_users')
        sqlunit.update_data(id_column='ach_name',
                            record='Moons',
                            param_column='Level',
                            param_val='0',
                            path='./misc/data.db',
                            table='hc_ach_users')

        row = sqlunit.read_db(column_name='id',
                              row_id=1,
                              path='./misc/data.db',
                              table='service')
        new_base = int(row[0][2]) * 2
        sqlunit.update_data(id_column='id',
                            record=1,
                            param_column='Value',
                            param_val=str(new_base),
                            path='./misc/data.db',
                            table='service')

    except:
        return None


def clear_text(message):
    text = re.sub(r'https?:\/\/.*[\r\n]*', '', message, flags=re.MULTILINE).strip()
    # Emojis pattern
    emoji_pattern = re.compile("["
                               u"\U0001F600-\U0001F64F"  # emoticons
                               u"\U0001F300-\U0001F5FF"  # symbols & pictographs
                               u"\U0001F680-\U0001F6FF"  # transport & map symbols
                               u"\U0001F1E0-\U0001F1FF"  # flags (iOS)
                               u"\U00002702-\U000027B0"
                               u"\U000024C2-\U0001F251"
                               u"\U0001f926-\U0001f937"
                               u'\U00010000-\U0010ffff'
                               u"\u200d"
                               u"\u2640-\u2642"
                               u"\u2600-\u2B55"
                               u"\u23cf"
                               u"\u23e9"
                               u"\u231a"
                               u"\u3030"
                               u"\ufe0f"
                               "]+", flags=re.UNICODE)
    text = emoji_pattern.sub(r'', text).strip()
    text = text.replace('\n', ' ').strip()
    if '@' in text or '/' in text or ' дня —' in text:
        text = ''
    try:
        if text[-1].isalpha() or text[-1].isdigit():
            text += '. '
        else:
            text += ' '
    except IndexError:
        pass
    return text


def moon_bank():
    with sqlite3.connect('./misc/data.db') as db:
        cursor = db.cursor()
        cursor.execute('SELECT SUM(Moons) FROM users')
        return cursor.fetchone()[0]


def add_separator(where_to_add):
    locale.setlocale(locale.LC_ALL, '')
    locale._override_localeconv = {'mon_thousands_sep': ' '}
    result = locale.format_string('%.0f', where_to_add, grouping=True, monetary=True)
    return result
